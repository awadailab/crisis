# Semantic and Visual Cues for Humanitarian Computing of Natural Disaster Damage Images
_______________________________________________________________________________________

## Dataset:
________
The dataset is available here: https://drive.google.com/open?id=1_u4Abj24NijDx1tWNiQj1M8ZbsMzMN-C 

## Citation: 
_________
H. S. Jomaa, Y. Rizk and M. Awad, "Semantic and Visual Cues for Humanitarian Computing of Natural Disaster Damage Images," 2016 12th International Conference on Signal-Image Technology & Internet-Based Systems (SITIS), Naples, 2016, pp. 404-411.
